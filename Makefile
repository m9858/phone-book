.PHONY: all, run, clean


all: main

main: main.o phoneBook.o cursor.o cbreak.o interfase.o
	g++ $^ -o Phonebook 


%.o: %.cpp
	g++ -c $< -o $*.o -Iheader 


run: main
	./Phonebook


clean: 
	rm -f *.o Phonebook

