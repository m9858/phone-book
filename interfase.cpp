#include <interfase.hpp>
#include <cursor.hpp>
#include <csignal>
#include <vector>

interfase::Contact::Contact(){
	cur::offcursor();
	signal(SIGINT, cur::exit);
}

interfase::Contact::~Contact(){
	this->_contact.~phoneBook();
}


int interfase::Contact::printButton(std::vector<std::string> button, int indent) const{
	int cursor = 0;
	char sumbol;
	do{
		for (int i = 0; i < button.size(); i++){
			if (cursor == i){
				std::cout << "\e[1;97;104m";
			}
			std::cout << button[i] << "\e[0m";
			this->repeat(" ", indent);
		}
		switch((sumbol = cur::input())){
			case 'D':
				cursor = ((cursor + 1) % button.size() + button.size()) % button.size();
				break;
			case 'A':
				cursor = ((cursor - 1) % button.size() + button.size()) % button.size();
				break;
		}
		std::cout << CLEAR << "\x1b[B";
	}while(sumbol != '\n');
	return cursor;
}


std::string* interfase::Contact::inputContc() const{
	cur::oncursor();
	std::string* contact = new std::string[3];
	std::cout << "Enter your name >> \e[3m";
	std::cin >> contact[0] ;
	std::cout << "\e[0mEnter your surname >> \e[3m";
	std::cin >> contact[1];
	std::cout << "\e[0mEnter your telephone >> \e[3m";
	std::cin >> contact[2];
	std::cout << "\e[0m";
	cur::offcursor();
	this->repeat(CLEAR, 4);
	return contact;
}


void interfase::Contact::repeat(const char* str, size_t len) const{
	while (len--){
		std::cout << str;
	}
}


void interfase::Contact::exit() const{
	this->~Contact();
	cur::exit(0);
}

void interfase::Contact::add(){
	std::vector<std::string> dataBut = {"Add another one", "Will come back"};
	std::cout << "\e[1;31m\tAdding a contact\e[0m" << std::endl;
	do{
		std::string* contc = this->inputContc();
		this->_contact.add(contc[0], contc[1], contc[2]);
		std::cout << std::endl;
		delete[] contc;
	}while(!this->printButton(std::vector<std::string> {"Add another one", "Will come back"}));
	std::cout << "\x1b[A" << CLEAR << "\x1b[B";
}	

bool interfase::Contact::help(){
	std::cout << "\e[1;31m\t\tManaging a stray\e[0m\e[3m" << std::endl; 
	std::cout << "You can delete using Backspace, " << "exit with esc," << std::endl;
	std::cout << "add a new contact with +, " << "search with q," << std::endl;
	std::cout << "you can scroll through the arrows or through (w, s, a, b)," << std::endl;
	std::cout << "open a hint on h, select if necessary using enter.\e[0m" << std::endl;
	const MyClass::user* con = this->_contact.phoneBookHead();
	int cursor = this->printButton(std::vector<std::string>  {"\x1b[C\x1b[C\x1b[CEntrance", "Exit"}, 39);
	this->repeat(CLEAR, 6);
	return cursor;
}

void interfase::Contact::contacts(){
	std::cout << "\x1b[B";
	const MyClass::user* tmpUser = this->_contact.phoneBookHead();
	while (true){
		std::cout << "\e[1;33m\t\tContacts\e[0m\e[0m" << std::endl; 
		const MyClass::user* tmp = tmpUser;
		if (tmp != nullptr){
			tmp = tmp->descendant == nullptr ? (tmp = tmp->ancestor != nullptr ? tmp->ancestor : tmp) : tmp;
			tmp = tmp->ancestor != nullptr ? tmp->ancestor : tmp;
			for (int i = 0; i < 3; i++){
				if (tmp != nullptr){
					std::cout << (tmpUser == tmp ?  "\e[1;97;104m" : "");
					std::cout << *tmp << "\e[0m";
					tmp = tmp->descendant;
				}
				std::cout << (i + 1 != 3 ? "\n" : "");
			}
		}
		else{
			std::cout << "\n\e[1;31m      There is not one contact yet\e[0m\n";
		}
		char sumbol = cur::input();
		this->repeat(CLEAR, 4);
		std::cout << "\x1b[B";
		if (sumbol == 'W' && tmpUser != nullptr ){

			if (tmpUser->ancestor != nullptr){
			       tmpUser = tmpUser->ancestor;
			}
		}
		else if (sumbol == 'S' && tmpUser != nullptr){
			if (tmpUser->descendant != nullptr){
			       tmpUser = tmpUser->descendant;
			}
		}
		else if (sumbol == '+'){
			this->add();
			tmpUser = this->_contact.phoneBookHead();
		}		
		else if (sumbol == 'q'){
			this->search();
		}
		else if (sumbol == 'h'){
			if (this->help()){
				std::cout << "\x1b[A";
				this->exit();
			}
			std::cout << "\x1b[B";
		}
		else if (sumbol == '\033'){
			std::cout << "\x1b[A";
			this->exit();
		}
		else if (sumbol == '\177'){
			tmpUser = this->_contact.remout(tmpUser);
		}
	}
}
	
void interfase::Contact::search() const{
	std::cout << "\e[1;31m\tSearch\e[0m" << std::endl;
	int cursor = 0;
	do{
		std::string* contc = this->inputContc();
		const MyClass::user* tmpUser = this->_contact.phoneBookHead();
		MyClass::user userPois = {nullptr, nullptr, contc[0], contc[1], contc[2]};
		delete[] contc;
		while (tmpUser!= nullptr){
			if ( userPois == *tmpUser){
				break;
			}
			tmpUser = tmpUser->descendant;
		}
		std::cout << std::endl;
		std::cout << (tmpUser == nullptr ? "\e[1;31m  The contact is mute\e[0m" : "\e[1;32m  The goal is in the list\e[0m") << std::endl;
		cursor = this->printButton(std::vector<std::string>  {"Try again", "Will come back"});
		std::cout << "\x1b[A" << CLEAR << "\x1b[B";
	}while(!cursor);
	std::cout << "\x1b[A" << CLEAR << "\x1b[B";
}	
