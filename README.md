# Phone book

## About the project

The project was created as a dz by МИСП.

The code is created on two-linked lists for storing contacts. 
There is also a break mechanism for control.

## Assembling

The cherz **make** program is being assembled and you will receive a Phonebook file at the output.

## Management

- You can delete using **Backspace**;
- exit with **esc**;
- add a new contact with **+**; 
- search with **q**;
- you can scroll through the arrows or through **(w, s, a, b)**;
- open a hint on **h**;
- szelect if necessary using **enter**.
