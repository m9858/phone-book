#include <cursor.hpp>
#include <cbreak.hpp>
#include <sys/select.h>


void cur::oncursor(){
	std::cout << "\x1b[?25h";
}

void cur::offcursor(){
	std::cout << "\x1b[?25l";
}

void cur::exit(const int ret){
	cbr::offcbreak();
	cur::oncursor();
	std::cout << std::endl;
	std::exit(ret != 0 ? 1 : 0);
}

char cur::input(){
        fflush(stdout);
	cbr::oncbreak();
	char symbol;
	read(0, &symbol, 1);
	if (symbol == 27){	
		fd_set x1, x2;
		FD_ZERO(&x1);
		FD_SET(0, &x1);
		FD_ZERO(&x2);
		struct timeval tm = {0, 5};
		if (select(1, &x1, &x2, &x2, &tm)){
			read(0, &symbol, 1);
			if (symbol == 91){
				read(0, &symbol, 1);
				if (symbol == 'A')
					symbol = 'W';
				else if (symbol == 'B')
					symbol = 'S';
				else if (symbol == 'C')
					symbol = 'D';
				else if (symbol == 'D')
					symbol = 'A';
			}
		}
	}
	cbr::offcbreak();
	return symbol;
}
