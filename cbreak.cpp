#include <cbreak.hpp>



termios cbr::getcbreak(){
	termios values;
	tcgetattr(0, &values);
	return values;
}

void cbr::oncbreak(){
	termios values = getcbreak();
	values.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &values);
}

void cbr::offcbreak(){
	termios values = getcbreak();
	values.c_lflag |= (ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &values);
}
