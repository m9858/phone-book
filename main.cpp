#include <interfase.hpp>
#include <iostream>

int main(){
	interfase::Contact contact;
	if (contact.help()){
		contact.exit(); 
	}
	contact.contacts();
	return 0;
}
