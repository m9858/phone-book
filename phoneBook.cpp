#include <phoneBook.hpp>

bool MyClass::user::operator<(const user& right) const{
	if (this->name < right.name){
		return true;
	}
	if (this->name == right.name && this->surname < right.surname){
		return true;
	}
	if (this->name == right.name && this->surname == right.surname && 
		this->telephone == right.telephone){
		return true;
	}
	return false;
}

bool MyClass::user::operator==(const user& right) const{
	return (this->name == right.name && 
	       this->surname == right.surname &&
       	       this->telephone == right.telephone);
}

std::ostream& operator<<(std::ostream& oun, const MyClass::user& other){
	return oun << other.name << " " << other.surname << ": " <<
		other.telephone;
}


MyClass::phoneBook::phoneBook(){
	this->head = nullptr;
}
	
MyClass::phoneBook::phoneBook(phoneBook&& other){
	this->head = other.head;
	other.head = nullptr;
}

MyClass::phoneBook::~phoneBook(){
	while (this->head != nullptr){
		user* tmp = this->head->descendant;
		delete this->head;
		this->head = tmp;
	}
}

MyClass::phoneBook::phoneBook(const phoneBook& other){
	user* tmpOther = other.head;
	this->head = nullptr;
	user* tmpThis = nullptr; 
	user* previous = nullptr;
	while (tmpOther != nullptr){
		tmpThis = new user;
		tmpThis->descendant = nullptr;
		tmpThis->ancestor = previous;
		tmpThis->name = tmpOther->name;
		tmpThis->surname = tmpOther->surname;
		tmpThis->telephone = tmpOther->telephone;
		if (previous != nullptr){
			previous->descendant = tmpThis;
		}
		previous = tmpThis;
		if (tmpOther->descendant == nullptr){
			break;
		}
		tmpOther = tmpOther->descendant;
		this->head = this->head == nullptr? tmpThis : this->head;
	}
}

const MyClass::user* MyClass::phoneBook::phoneBookHead() const{
	return this->head;
}

void MyClass::phoneBook::add(std::string name, std::string surname, std::string telephone){
	user* newUser = new user;
	newUser->name = name;
	newUser->surname = surname;
	newUser->telephone = telephone;
	if (this->head == nullptr){
		newUser->descendant = nullptr;
		newUser->ancestor = nullptr;
		this->head = newUser;
		return;
	}
	user* tmp = this->head;
	while (true){
		if (*newUser  < *tmp){
			newUser->descendant = tmp;
			newUser->ancestor = tmp->ancestor;
			if (tmp->ancestor != nullptr){
				tmp->ancestor->descendant = newUser;
			}
			this->head = this->head == tmp ? newUser : this->head;
			tmp->ancestor = newUser;
			return;
		}	
		if (tmp->descendant == nullptr){
			newUser->ancestor = tmp;
			newUser->descendant = nullptr;
			tmp->descendant = newUser;
			return;
		}
		tmp = tmp->descendant;
	}
}

bool MyClass::phoneBook::remout(std::string name, std::string surname, std::string telephone){
	user* tmp = this->head;
	while(tmp != nullptr){
		if (*tmp == (user){nullptr, nullptr, name, surname, telephone}){
			if(tmp->ancestor != nullptr){
				tmp->ancestor->descendant = tmp->descendant; 
			}
			if(tmp->descendant != nullptr){
				tmp->descendant->ancestor = tmp->ancestor;
			}
			this->head = tmp->ancestor == nullptr? tmp->descendant : this->head;
			delete tmp;
			return true;
		}
		if (tmp->descendant == nullptr){
			return false;
		}
		tmp = tmp->descendant;
	}		
	return false;
}

const MyClass::user* MyClass::phoneBook::remout(const MyClass::user* other){
	if (other == nullptr){
		return nullptr;
	}
	if (other->ancestor != nullptr){
		other->ancestor->descendant = other->descendant; 
	}
	if(other->descendant != nullptr){
		other->descendant->ancestor = other->ancestor;
	}
	this->head = other->ancestor == nullptr? other->descendant : this->head;	
	user* ancestor = other->ancestor;
	user* descendant = other->descendant;
	delete other;
	return descendant != nullptr ? descendant : ancestor;
}
