#pragma once
#include <phoneBook.hpp>
#include <vector>

namespace interfase{
	class Contact{
		private:
			MyClass::phoneBook _contact;
			void repeat(const char* str, size_t len) const;
			int printButton(std::vector<std::string> data, int indent = 2) const;
			std::string* inputContc() const;
			void add();
			void search() const;
			
		public:
			Contact();
			~Contact();
			bool help();
			void contacts();
			void exit() const;
	};
};
