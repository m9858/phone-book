#pragma once
#include <termios.h>
#include <unistd.h>

namespace cbr{
	termios getcbreak();
	void offcbreak();
	void oncbreak();
}


