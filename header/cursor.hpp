#pragma once
#include <iostream>

#define CLEAR "\r\33[2K\x1b[A"

	
namespace cur{
	void oncursor();
	void offcursor();
	void exit(const int ret);
	char input();
	std::string* add();
};
