#pragma once
#include <string>
#include <ostream>

namespace MyClass{
	struct user{
		user* descendant;//потом
		user* ancestor;//родитель
		std::string name;
		std::string surname;
		std::string telephone;
		bool operator<(const user& right) const;//+
		bool operator==(const user& right) const;//+
	};

	class phoneBook{
		private:
			user* head;
		public:
			phoneBook();//+
			phoneBook(phoneBook&& other);//+
			phoneBook(const phoneBook& other);//+
			~phoneBook();//+
			const user* phoneBookHead()const;//+
			void add(std::string name, std::string surname, std::string telephone);//+
			bool remout(std::string name, std::string surname, std::string telephone);//+
			const user* remout(const user* other);//+
	};
}


std::ostream& operator<<(std::ostream& oun, const  MyClass::user& other);
